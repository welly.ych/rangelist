# RangeList

## To Run

- run bundler
```
bundle install
```

- run the tests
```
bundle exec rspec
```

## 思路

1. 开始没有好的思路，直接使用暴力求解，实现为 [RangeList](lib/range_list.rb)，时间复杂度 O(n)，空间复杂度 O(n)，n 为 range 数量而非值域
2. 检索资料有线段树和珂朵莉树可以对区间进行检索，原理尚未理解，暂未实现
3. 如果值域范围确定且较小，可以直接暴力展开为Set，用空间换时间，时间复杂度为 O(n)，空间复杂度为 O(n)，n 为值域范围
