describe RangeList do
  let(:rl) { rl = RangeList.new }
  shared_context 'with some ranges' do
    ranges = [[1, 5], [8, 12], [15, 20]]
    let(:ranges) { ranges }
    let(:rl) {
      RangeList.new.tap do |obj|
        ranges.each { |range| obj.add(range) }
      end
    }
  end
  subject { rl.ranges }

  describe ".add" do
    context "when a RangeList adds an invalid range" do
      include_context "with some ranges"
      it "does nothing" do
        rl.add(nil)
        is_expected.to eq(ranges)
        rl.add('[1, 2]')
        is_expected.to eq(ranges)
        rl.add([])
        is_expected.to eq(ranges)
        rl.add([1, 2, 3])
        is_expected.to eq(ranges)
        rl.add([3, 1])
        is_expected.to eq(ranges)
      end
    end

    context "when a RangeList adds first range" do
      it "contains one range" do
        rl.add([1, 5])
        is_expected.to eq([[1, 5]])
      end
    end

    context "when a RangeList adds a range with no intersection" do
      include_context "with some ranges"
      it "appends the range to ranges" do
        rl.add([30, 40])
        is_expected.to eq(ranges + [[30, 40]])
      end
      it "prepends the range to ranges" do
        rl.add([-10, -5])
        is_expected.to eq([[-10, -5]] + ranges)
      end
      it "inserts the range to ranges" do
        rl.add([6, 7])
        is_expected.to eq(ranges.insert(1, [6, 7]))
      end
    end

    context "when a RangeList adds a range with intersection" do
      include_context "with some ranges"
      it "replaces the left of a range" do
        rl.add([6, 10])
        is_expected.to eq([[1, 5], [6, 12], [15, 20]])
      end
      it "replaces the right of a range" do
        rl.add([10, 14])
        is_expected.to eq([[1, 5], [8, 14], [15, 20]])
      end
      it "replaces both the left and the right of a range" do
        rl.add([6, 14])
        is_expected.to eq([[1, 5], [6, 14], [15, 20]])
      end
      it "merges some ranges" do
        rl.add([10, 18])
        is_expected.to eq([[1, 5], [8, 20]])
      end
      it "merges all ranges" do
        rl.add([3, 18])
        is_expected.to eq([[1, 20]])
      end
    end
  end

  describe ".remove" do
    context "when a RangeList removes an invalid range" do
      include_context "with some ranges"
      it "does nothing" do
        rl.remove(nil)
        is_expected.to eq(ranges)
        rl.remove('[1, 2]')
        is_expected.to eq(ranges)
        rl.remove([])
        is_expected.to eq(ranges)
        rl.remove([1, 2, 3])
        is_expected.to eq(ranges)
        rl.remove([3, 1])
        is_expected.to eq(ranges)
      end
    end

    context "when a RangeList removes a range which is not included" do
      include_context "with some ranges"
      it "does nothing" do
        rl.remove([30, 40])
        is_expected.to eq(ranges)
      end
    end

    context "when a RangeList removes a range which is included" do
      include_context "with some ranges"
      it "removes the range only" do
        rl.remove([1, 5])
        is_expected.to eq(ranges - [[1, 5]])
      end
    end

    context "when a RangeList removes a range with intersection" do
      include_context "with some ranges"
      it "replaces the left of a range" do
        rl.remove([6, 10])
        is_expected.to eq([[1, 5], [10, 12], [15, 20]])
      end
      it "replaces the right of a range" do
        rl.remove([10, 14])
        is_expected.to eq([[1, 5], [8, 10], [15, 20]])
      end
      it "removes a range" do
        rl.remove([6, 14])
        is_expected.to eq([[1, 5], [15, 20]])
      end
      it "removes a range and replaces the left of a range" do
        rl.remove([6, 18])
        is_expected.to eq([[1, 5], [18, 20]])
      end
      it "removes a range and replaces the right of a range" do
        rl.remove([3, 14])
        is_expected.to eq([[1, 3], [15, 20]])
      end
    end
  end
end
