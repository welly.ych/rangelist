# Task: Implement range_left class named 'RangeList'
# A pair of integers define range_left range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
class RangeList
  attr_reader :ranges

  def initialize
    @ranges = []
  end

  def add(range)
    return unless valid?(range)
    return if range[0] == range[1]
    idx = @ranges.bsearch_index { |elem| elem[0] > range[0] } || @ranges.size
    @ranges.insert(idx, range)
    compact!
  end

  def remove(range)
    return unless valid?(range)
    return if range[0] == range[1]
    range_left, range_right = range
    i = 0
    while i < @ranges.size
      cur_left, cur_right = @ranges[i]
      break if range_right <= cur_left
      if range_left < cur_right
        if cur_left < range_left
          @ranges[i] = [@ranges[i][0], range_left]
          if range_right < cur_right
            @ranges.insert(i + 1, [range_right, cur_right])
            break
          end
        else
          if cur_right <= range_right
            @ranges.delete_at(i)
            next
          else
            @ranges[i] = [range_right, @ranges[i][1]]
            break
          end
        end
      end
      i += 1
    end
  end

  def print
    puts @ranges.map { |elem| "[#{elem[0]}, #{elem[1]})" }.join(" ")
  end

  private

  def valid?(range)
    range.is_a?(Array) && range.size == 2 && range.all? { |elem| elem.is_a?(Integer) } && range[0] < range[1]
  end

  def compact!
    i = 1
    while i < @ranges.size
      prev_right = @ranges[i - 1][1]
      cur_left, cur_right = @ranges[i]
      if prev_right >= cur_right
        @ranges.delete_at(i)
      elsif prev_right >= cur_left
        @ranges.delete_at(i)
        @ranges[i - 1][1] = cur_right
      else
        i += 1
      end
    end
  end
end
